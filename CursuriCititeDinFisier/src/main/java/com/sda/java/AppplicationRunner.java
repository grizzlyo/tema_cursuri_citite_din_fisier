package com.sda.java;

import java.util.Map;

public class AppplicationRunner {
    public static void main(String[] args) {

        Logic logic = new Logic();

        for (Map.Entry<Cursuri,Integer> entry : logic.citescCursuriDinFisier(Const.PATH).entrySet())
            System.out.println(entry.getKey() +
                    ", Nr. de aparitii = " + entry.getValue());

    }
}
