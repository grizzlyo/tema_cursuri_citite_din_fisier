package com.sda.java;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class Logic {

    public Map<Cursuri, Integer> citescCursuriDinFisier(String path) {
        File file = new File(path);
        Map<Cursuri, Integer> map = new HashMap<>();

        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);

            String linie = reader.readLine();
            String[] arr = null;
            int nrAparitii = 0;

            while (linie != null) {
                Cursuri cursuri = new Cursuri();
                arr = linie.split(" ");

                cursuri.setId(Integer.valueOf(arr[0]));
                cursuri.setNume(arr[1]);
                cursuri.setLungime(Integer.valueOf(arr[2]));
                cursuri.setAn(Integer.valueOf(arr[3]));

                linie = reader.readLine();

                if (!map.containsKey(cursuri)) {
                    map.put(cursuri, 1);
                } else {
                    nrAparitii = map.get(cursuri);
                    nrAparitii++;
                    map.put(cursuri, nrAparitii);
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return map;
    }
}
