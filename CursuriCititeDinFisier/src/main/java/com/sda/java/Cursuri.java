package com.sda.java;

import java.util.Objects;

public class Cursuri {

    private int id;
    private String nume;
    private int lungime;
    private int an;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public int getLungime() {
        return lungime;
    }

    public void setLungime(int lungime) {
        this.lungime = lungime;
    }

    public int getAn() {
        return an;
    }

    public void setAn(int an) {
        this.an = an;
    }

    @Override
    public String toString() {
        return "Cursuri programare IT{" +
                "id =" + id +
                ", nume curs='" + nume + '\'' +
                ", total ore =" + lungime +" ore"+
                ", an desfasurare =" + an +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cursuri)) return false;
        Cursuri cursuri = (Cursuri) o;
        return getId() == cursuri.getId() &&
                getLungime() == cursuri.getLungime() &&
                getAn() == cursuri.getAn() &&
                getNume().equals(cursuri.getNume());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getNume(), getLungime(), getAn());
    }
}
